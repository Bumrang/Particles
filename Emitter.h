#include <glfw.h>
#include <vector>
#include <math.h>
#include "Particle.h"

#ifndef EMITTER
#define EMITTER

class Emitter
{
public: 
	float x , y , vx , vy; // The particle's x's, y's, and velocities.
	unsigned int sections; // This is the amount of sections the "circle" will have.
	float radius; // radius of the circle
	float Pi;
	float TwoPi; // Lets go get two pies from the shop, apple probably. No, what do you think this is?
	float Time; // the time
	float red , green , blue; // rgb
	unsigned int width, height; 
	float Rad;
	int life;
	bool color;
	int maxsize;
	bool mainEmitter;
	int posX, posY; // this is for the particle fountain
    int curMX , curMY; // this always keeps track of the mouse
	float Degrees;
	float Radians;


	std::vector <Particle> Particles;
	

	Emitter(float X , float Y , float VX , float VY , bool color , int Width , int Height , bool mainrmitter);
	void Draw (bool mouse);
	void Update (float);
	void Stop (bool mouse , int , int);
	void Fire (float);
	
	float frand(float , float);
};

#endif