#include "Emitter.h"

Emitter::Emitter(float X , float Y , float VX , float VY , bool Color , int Width , int Height , bool mainemitter)
{
	posX = X;
	posY = Y;
	vx = VX;			// Assign all of the xy's and velocities to what is passed in.
	vy = VY;

	width = Width;
	height = Height;

	sections = 20; // sections for the circle
	radius = 2.0f;          // radius
	Pi = 3.14159;
	TwoPi = 2 * 3.14159; // for drawing the circle
	life = 1;
	color = Color;
	maxsize = 1000;
	mainEmitter = mainemitter;
	

	if (color) // alternate colors, every other is red or orange
	{
		red = 1.0f; green = 0.0f; blue = 0.0f; // red
	}
	
	else 
	{
		red = 1.0f; green = 0.5f; blue = 0.0f; // orange
	}
}

void Emitter::Draw(bool mouse)
{
	Degrees = rand()%360;
	Radians = Degrees/180.f*Pi;

	if (color)
			color = false;

		else
			color = true;


		if (Particles.size() <= maxsize)
		{
			if (mainEmitter)
			{		
				glfwGetMousePos(&posX,&posY);
			}

			Particles.push_back(Particle(posX,
				-posY+height,
				cos (Radians) * frand (140 , -140),
				sin (Radians) * frand (140 , -140),
				color,
				width,
				height));

			for (int x = 0; x < Particles.size(); x++) 
			{
				Particles[x].Draw();
			}
		}
}

void Emitter::Update(float DeltaTime)
{
		for (int x = 0; x < Particles.size(); x++)
		{
			if (Particles[x].Update(DeltaTime))
			{
				Particles.erase(Particles.begin() + x);
			}
		}
}

void Emitter::Stop(bool mouse , int curx , int cury)
{
	if (mouse)
	{
		mouse = false;

		posX = curx;
		posY = cury;
	}
	else
		mouse = true;
}

void Emitter::Fire(float DeltaTime)
{
	int ranpar = frand (Particles.size() , 0);

	
					
	glfwGetMousePos(&curMX , &curMY);
					
	Particles[ranpar].Fire(curMX , curMY , posX , posY , DeltaTime); 
	Particles[ranpar].green = 1.0f;
	Particles[ranpar].life = 4;
					
}

float Emitter::frand(float high , float low)
{
	return low + (float)(rand()%100/100.f) * (high - low);
}