#include "Particle.h"

Particle::Particle(float X , float Y , float VX , float VY , bool color , int Width , int Height)
{
	x = X;
	y = Y;
	vx = VX;			// Assign all of the xy's and velocities to what is passed in.
	vy = VY;

	width = Width;
	height = Height;

	sections = 20; // sections for the circle
	radius = 2.0f;          // radius
	Pi = 3.14159;
	TwoPi = 2 * 3.14159; // for drawing the circle
	life = 1;

	Time = glfwGetTime(); // time, for deletion and such

	if (color) // alternate colors, every other is red or orange
	{
		red = 1.0f; green = 0.0f; blue = 0.0f; // red
	}
	
	else 
	{
		red = 1.0f; green = 0.5f; blue = 0.0f; // orange
	}
}

void Particle::Draw()
{
	glPushMatrix();
    glTranslatef(x,y,0);
    glBegin(GL_TRIANGLE_FAN);

	glColor3f (red , green , blue); 
	glVertex2f(0.0f , 0.0f); // The origin.

	for (int i = 0; i <= sections; i++) // Draw the "circle".
	{
		 glVertex2f(radius * cos(i *  TwoPi / sections) , radius* sin(i * TwoPi / sections)); 
	}
	


	/*	glPointSize(10);
		glBegin(GL_POINTS);  // this is just for testing drawing
		glVertex2f(x,y);*/
		


	glEnd();
	glPopMatrix();
}

bool Particle::Update(float DeltaTime)
{

	if (x - 5 <= 0)
	{
		x = 6;
		vx *= -1;
	}
		
	if (x + 5 >= width)
	{
		x = width - 6;
		vx *= -1;
	}

	if (y - 5 <= 0)
	{
		y = 6;
		vy *= -1;
	}

	if (y + 5 >= height)
	{
		y = height - 6;
		vy *= -1;
	}

	x += vx * DeltaTime; // the particle's next positions
	y += vy * DeltaTime;

	/*if (glfwGetTime() - Time > 1 && red < 1.0f && red > 0.0f) // reds
		red += 0.1f;
	else if (red >= 1.0f)
		red = 0.0f;

	if (glfwGetTime() - Time > 1 && green < 1.0f && green > 0.0f) // greens                            // not sure how to implement this but keeping it here just incase.
		green += 0.0001f;
	else if (green >= 1.0f)
		green = 0.0f;

	if (glfwGetTime() - Time > 1 && blue < 1.0f && blue > 0.0f) // blues
		blue += 0.0001f;
	else if (blue >= 1.0f)
		blue = 0.0f; */

	if (glfwGetTime() - Time > life) // if it lasts for that certain amount of time, too bad for the particle.
		return true;

	else
		return false;
}


void Particle::Fire (int MouseX , int MouseY , int OMX ,  int OMY , float DeltaTime) 
{

	 Rad = atan2((float)MouseX-(float)OMX,(float)MouseY-(float)OMY);
	 Rad += 3 * (Pi/2);
	vx = (cos(Rad) * 70000) * DeltaTime;
	vy = (sin(Rad) * 70000) * DeltaTime;
}