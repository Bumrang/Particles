#include <glfw.h>
#include <iostream>
#include <vector>
#include "Particle.h"
#include "Emitter.h"
#include "Keys.h"

static unsigned int Width = 800;
static unsigned int Height = 600;


float frand(float high , float low)
{
	return low + (float)(rand()%100/100.f) * (high - low);
}

int main()
{
	 

	if (!glfwInit())
   {
      std::cout << "GLFW failed to initialize!\n";
      return 1;
   }
   glfwOpenWindow(Width,Height,0,0,0,0,0,0,GLFW_WINDOW);
   glfwSetWindowTitle("Particles!");
   glViewport(0,0,Width, Height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glOrtho(0,Width,0,Height,0,1);
   glMatrixMode(GL_MODELVIEW);
   glClearColor(0,0,0,1);

   std::vector<Emitter> Emitters;
   unsigned int maxsize = 1000;
   bool color = true;
   bool mouse = true; 
   int X,Y; // this is for the particle fountain
   int curMX , curMY; // this always keeps track of the mouse

   bool SPressed = false;
   bool SReleased = true;
   bool NPressed = false;
   bool NReleased = true;
   bool BackPressed = false;
   bool BackReleased = true;
   bool MainLoaded = false;
   bool MainEmitter = true;

   float r = 1.0f; float g = 0.0f; float b = 0.0f;
   float Pi = 3.14159;
    
   
	Keys Key;
	Key.Init(); // init all the keys, aka make them false.

   float LastTime = glfwGetTime(); // delta time
   while (true)
   {
	 // int ranpar = frand (Particles.size() , 0);
	float Degrees = rand()%360;
		float Radians = Degrees/180.f*Pi;

		
	   if (!MainLoaded)
	   {
		   glfwGetMousePos(&X , &Y);

		   Emitters.push_back(Emitter(X,
						Y,
						cos (Radians) * frand (140 , -140),
						sin (Radians) * frand (140 , -140),
						color,
						Width,
						Height,
						MainEmitter));

		   MainLoaded = true;
		   MainEmitter = false;
	   }
	   
	   float DeltaTime = glfwGetTime()-LastTime; // delta time
	   LastTime = glfwGetTime(); // delta time

	   



	    if (!glfwGetWindowParam(GLFW_OPENED)) // Closed the window? That's pretty cool.
		 {
           std::cout << "Window was closed! Exiting...\n";
           break;
		 }

			if (Key.SinglePress('S'))
				{
					glfwGetMousePos (&curMX , &curMY); 
					Emitters[0].Stop(mouse , curMX , curMY);

					std::cout << "S was pressed.\n";

					
				}


			/*	if (!mouse)
				{
					for (int x = 0; x <= Emitters.size(); x++)
					{
						Emitters[x].Fire(DeltaTime);
					}
				}*/

		 if (Key.SinglePress('N'))
		 {
					glfwGetMousePos(&X, &Y);

					Emitters.push_back(Emitter(X,
						Y,
						cos (Radians) * frand (140 , -140),
						sin (Radians) * frand (140 , -140),
						color,
						Width,
						Height,
						MainEmitter));

					std::cout << "N was pressed \n.";
		 }

				if (glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT) && !glfwGetKey('N'))
				{
					for (int i = 1; i < Emitters.size(); i++)
						Emitters[i].Fire(DeltaTime);
				}

			if (Key.SinglePress(GLFW_KEY_BACKSPACE))
				{
					if (Emitters.size() > 0)
					{
						Emitters.pop_back();

						std::cout << "Backspace was pressed\n";
					}
				}
			

		  
		for (int x = 0; x < Emitters.size(); x++)
		{
			Emitters[x].Update(DeltaTime);
		}
		// Updates above clear.
		glClear(GL_COLOR_BUFFER_BIT);
		// Draws under clear.

	/*	if (color)
			color = false;

		else
			color = true; */


		/*if (Particles.size() <= maxsize)
		{
			if (mouse)
			{		
				glfwGetMousePos(&X,&Y);
			}

			Particles.push_back(Particle(X,
				-Y+Height,
				cos (Radians) * frand (140 , -140),
				sin (Radians) * frand (140 , -140),
				color,
				Width,
				Height));*/

			for (int x = 0; x < Emitters.size(); x++) 
			{
				Emitters[x].Draw(mouse);
			}
		//}
		glfwSwapBuffers();
   }

}