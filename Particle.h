#include <glfw.h> 
#include <math.h>

#ifndef PARTICLES
#define PARTICLES

class Particle // One, single particle.
{
public: 

	float x , y , vx , vy; // The particle's x's, y's, and velocities.
	unsigned int sections; // This is the amount of sections the "circle" will have.
	float radius; // radius of the circle
	float Pi;
	float TwoPi; // Lets go get two pies from the shop, apple probably. No, what do you think this is?
	float Time; // the time
	float red , green , blue; // rgb
	unsigned int width, height; 
	float Rad;
	int life;

	Particle (float , float , float , float , bool , int , int); // init 
	void Draw (); // Draw this single particle.
	bool Update (float); // Move this particle and THEN check if it is past the Y coordinate.
	void Fire (int , int , int , int , float); // Fire function, for firing a particle.
};


#endif